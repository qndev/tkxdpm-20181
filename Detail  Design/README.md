Phân công công việc Homework 03
===============================

- - -

* ** Nguyễn Đình Quang **
    * 1.1: Vẽ biểu đồ Package-Sequence-Class-Communication Diagram Detial Login User, một số diagram CRUD General management(review Yos Saroeun)
    * 1.2: Vẽ biểu đồ E-R Diagram(kết hợp với Yos Saroeun) + Review Diagram Admin của Nguyễn Văn Quyết, Thiết kế giao diện
    * 1.3: Viết các đặc tả đi kèm các Diagram của mình trong file word Software Design Description.doc
    * 1.4: Tổng hợp các đặc tả của các thành viên và viết file báo cáo Software Design Description Final.docx

* ** Nguyễn Văn Quyết **
    * 1.1: Vẽ biểu đồ Admin Diagram CRUD, Viết các đặc tả đi kèm các Diagram của mình, Review Package Diagram Total của Nguyễn Đình Quang
    * 1.2: Vẽ table database cho Admin CRUD
    * 1.3: Thiết kế giao diện 

* ** Yos Saroeun **
    * 1.1: Vẽ biểu đồ Sequence Diagram Student Management CRUD, Review Package Diagram Total
    * 1.2: Viết các đặc tả đi kèm các Diagram của mình
    * 1.3: E-R Diagram(Nguyễn Đình Quang), Thiết kế giao diện

* ** Kerlor Senglao **
    * 1.1: Sequence Diagram Admin CRUD (Review Nguyễn Văn Quyết, Review Package Diagram Total Nguyễn Đình Quang)
    * 1.2: Viết các đặc tả đi kèm các Diagram của mình
    * 1.3: Thiết kế giao diện

* ** Kerlor Senglao vs Yos Saroeun chủ yếu làm việc về phần  Achitectural Design **

- - -

 |       Họ tên     | MSSV     |
 |:-----------------|:--------:|
 |Nguyễn Đình Quang | 20146574 |
 |Nguyễn Văn Quyết  | 20153075 | 
 |Yos Saroeun       | 20154461 | 
 |Kerlor Senglao    | 20154474 | 
 

