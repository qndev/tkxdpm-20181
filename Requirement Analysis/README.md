Phân công công việc
====================

- - -

* ** Nguyễn Đình Quang **
    * 1.1: Biểu đồ use case phân rã, tổng quan (Phân rã use case Administration),(Phân rã  UseCase Diagram General management vs Student)
    * 1.2: Biểu đồ Activity Diagram nhân tố đăng nhập và sử dụng hệ thống(Activity Diagram User Login + (Yos Saroeun)) 
    * 1.3: Biểu đồ Sequence Diagram Login System
    * 1.4: Viết các đặc tả đi kèm các Use Case, Activity Diagram có trong file word của thư mục Nguyen Dinh Quang
    * 1.5: Thu thập các bản đặc tả của các thành viên và viết báo các đặc tả cho hệ thống SIS

* ** Nguyễn Văn Quyết **
    * 2.1: Biểu đồ hoạt động mở đăng kí lớp học đóng đăng kí và gia hạn thời gian đăng kí học
    * 2.2: Biểu đồ hoạt động cập nhật thông tin sinh viên, tạo tài khoản User(Student)
    * 2.3: Biểu đồ hoạt động sử lí điểm sinh viên (Activity Diagram Point management)
    * 2.4: Viết các đặc tả đi kèm các Activity Diagram có trong file word của thư mục Activity Diagram

* ** Yos Saroeun **
    * 3.1: Biểu đồ hoạt động cập nhật lớp học UpdateCourse_ActivityDiagram
    * 3.2: Biểu đồ hoạt động User Login Activity, trình tự Sequence Diagram + (Nguyễn Đình Quang )
    * 3.3: Viết các đặc tả đi kèm các Activity Diagram có trong file word của thư mục Yos Saroeun

* ** kERIOR SENGLAO **
    * 4.1: Biểu đồ hoạt động xóa các lớp học đã đăng kí của sinh viên
    * 4.2: Viết các đặc tả chức năng CRUD khóa học + lớp học của tác nhân General Manament

- - -

 |       Họ tên     | MSSV     |
 |:-----------------|:--------:|
 |Nguyễn Đình Quang | 20146574 |
 |Nguyễn Văn Quyết  | 20153075 | 
 |Yos Saroeun       | 20154461 | 
 |kERIOR SENGLAO    | 20154474 | 
 

