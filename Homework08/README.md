Phân công công việc Homework 08
===============================

- - -

* ** Nguyễn Văn Quyết **
    * 1.1: Vẽ Class Diagram: StudentDAO, ClassDAO, SubjectsDAO, AddUserDAO, IAddUserDAO, IStudentDAO.
    * 1.2: Viết các document cho các class diagram đã vẽ.

* ** Yos Saroeun **
    * 1.1: Vẽ Class Diagram:Tuition, Check, Schedule
    * 1.2: Viết các document cho các class diagram đã vẽ.

* ** Kerlor Senglao **
    * 1.1: Vẽ Class Diagram:DBConnect, ExportData, SearchInfoStudent, TuitionStudent.
    * 1.2: Viết các document cho các class diagram đã vẽ.

* ** Nguyễn Đình Quang **
    * 1.1: Vẽ Class Diagram còn lại
    * 1.2: Viết các document cho các class diagram đã vẽ.
    * 1.3: Data Modeling: Thiết kế database.
* ** Cấu trúc và tất cả các class diagram có trong hình ảnh minh họa: Class_Explorer.png **

- - -

 |       Họ tên     | MSSV     |
 |:-----------------|:--------:|
 |Nguyễn Đình Quang | 20146574 |
 |Nguyễn Văn Quyết  | 20153075 | 
 |Yos Saroeun       | 20154461 | 
 |Kerlor Senglao    | 20154474 | 
 
