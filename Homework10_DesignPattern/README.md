Homework10 - Design Patterns - Principles
=========================================

- - -

* ** Nguyễn Đình Quang **
    *  Memento Pattern (File: Design Patterns Presentation.pdf, Design Patterns Presentation.odp)
    *  Report Desgin Principles (File: Report Design Principles.pdf)

* ** Nguyễn Văn Quyết **
    *  Visitor Design Pattern (File: visitor-design-pattern.pptx, Visitor-Design-Pattern.docx)

* ** Yos Saroeun **

* ** Kerlor Senglao **
    *  Decorator pattern (File: DecoratorPattern-KerlorSenglao.pptx)

- - -

 |       Họ tên     | MSSV     |
 |:-----------------|:--------:|
 |Nguyễn Đình Quang | 20146574 |
 |Nguyễn Văn Quyết  | 20153075 | 
 |Yos Saroeun       | 20154461 | 
 |Kerlor Senglao    | 20154474 | 
 

