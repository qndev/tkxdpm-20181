Phân công công việc
====================

- - -

* ** DateUtil **
    * 1.1: boolean  checkDate(int  day,  int  month,  in  year): check if the input is valid or not.
    * 1.2: int calculateAge(intyear):  return the age of the person having the birthYear; using  java.util.Date to get the current date.

* ** NumberUtil **
    * 2.1: Check if an integer is a prime number or a composite.
    * 2.2: Check if an integer is a square number.
    * 2.3: Check if an integer is a perfect number.

* ** HelloWorld **
    * 3.1: Ask a user to  enter his/her name and birthday.
    * 3.2: If  the  birthday  is  not  a  valid  date,  ask  her  again  until  it  is.
    * 3.3: Say hello to that person and display his/her age.

* ** FirstNumberApp **
    * 4.1: Ask user to enter an integer from keyboard, if not ask her/him to enter again until valid
    * 4.2: Use NumberUtilclass
        * 4.2.1: Display if the number is a prime or composite 
        * 4.2.1: Display if the number is a square number
        * 4.2.3: Display if the number is a perfect number
  
* ** Phân công các task của từng thành viên trong nhóm** 

- - -

 |       Họ tên     | MSSV       |      Tasks       |
 |:-------------------:|:-------:|:---------------: |
 |Nguyễn Đình Quang | 20146574 | 1.1, 1.2, 3.1, 3.2 |
 |Nguyễn Văn Quyết  | 20153075 | 2.1, 2.2, 4.2      |
 |Yos Saroeun       | 20154461 | 1.2, 2.3, 4.1      |
 |kERIOR SENGLAO    | 20154474 | 3.3, 4.2           |
 
