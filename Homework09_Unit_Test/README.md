Phân công công việc Homework 09
===============================

- - -

* ** Nguyễn Văn Quyết **
    * 1.1: Lập trình các class testsuite sử dụng Junit:ManagerUserTest.java, ChangePasswordTest.java

* ** Yos Saroeun **
    * 1.1: Lập trình các class testsuite sử dụng Junit:AdminLoginTest.java, ScheduleTest.java

* ** Kerlor Senglao **
    * 1.1: Lập trình các class testsuite sử dụng Junit:SearcjInfoStudentTest.java 

* ** Nguyễn Đình Quang **
    * 1.1: Thiết kế các test case(review cho Quyết, Saroeun, Senglao)
    * 1.2: Lập trình các class testsuite sử dụng Junit:StudentLoginTest.java, CreateCoursesTest.java

* ** Các Test Case được miêu tả trong file Unit_test.xlsx hoặc file Unit_test.pdf **
- - -

 |       Họ tên     | MSSV     |
 |:-----------------|:--------:|
 |Nguyễn Đình Quang | 20146574 |
 |Nguyễn Văn Quyết  | 20153075 | 
 |Yos Saroeun       | 20154461 | 
 |Kerlor Senglao    | 20154474 | 
 
