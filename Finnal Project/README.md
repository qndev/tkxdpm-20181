Finnal Project 
===============================

- - -

1.Kiến trúc

*  Mô hình n-tier 3-tier 3-layer
*  Thành phần cấu trúc:
   *  Presentation Layer (GUI)
   *  Business Logic Layer (BLL)
   *  Data Access Layer (DAL) 
   
2.Nguyên lý thiết kế đã áp dụng:

* The Single Responsibility Principle - Nguyên lý đơn nhiệm.
   * Một class chỉ nên có một trách nhiệm duy nhất.
* The Open Closed principle - Nguyên lý mở rộng/hạn chế.
   * Các thực thể phần mềm (các lớp, mô-đun, chức năng, v.v.) nên được Open để mở rộng, nhưng Closed để sửa đổi.
* The Interface Segregation Principle - Nguyên lý phân tách giao tiếp.
   * Phân tách các Interface lớn thành nhiều các interface nhỏ với mục đích cụ thể. Khi đó việc implement và quản lý sẽ dễ hơn.

3.Phân công công việc: 

* Công việc chung của các thành viên: Thiết kế giao diện, bàn luận thiết kế CSDL
* Công việc riêng
  * Nguyễn Văn Quyết:
    * Đổi Mật khẩu Sinh viên (F1)
    * Quản lý Thông tin sinh viên (F2)
    * Xem danh sách đăng kí lớp học (F3) 
    * Quản lý USer Account(Xóa, cập nhật) (F4)
  * Yos Saroeun 
    * Đăng nhập của Admin (F5)
    * Tra cứu Học phí SV (F6)
    * Tra cứu thời khóa biểu SV (F7)
  * Kerlor Senglao
    * Cập nhật thông tin sinh viên (của 1 sinh viên sau khi đăng nhập) (F8)
    * Tra cứu Thông tin SV (F9)
    * Tra cứu Thông tin môn học (F10)
  * Nguyễn Đình Quang 
    * Quản lý USer Account(Thêm mới account) (F11)
    * Sinh viên đăng kí học tập (đang kí và hủy đăng kí lớp học) (F12)
    * Quản lý đăng kí lớp học (thêm, sửa, xóa, cập nhật) (F13)
    * Đăng nhập của Student (F1)
    * Thiết kế chính các bảng, thuộc tính trong CSDL
    * Viết báo cáo; review Yos Saroeun(F5), Nguyễn Văn Quyết(F1), Kerlor Senglao(F8)
    
4.Kiểm thử :

* Test techniques: 
   * Black Box Testing
     * Phân vùng tương đương
     * Bảng quyết định
   * Unit Testing

* ** Nguyễn Văn Quyết **
* 1.1: Lập trình các class testsuite sử dụng Junit:StudentLoginTest.java, SearcjInfoStudentTest.java

* ** Yos Saroeun **
* 1.1: Lập trình các class testsuite sử dụng Junit:ChangePasswordTest.java, ManagerUserTest.java

* ** Kerlor Senglao **
* 1.1: Lập trình các class testsuite sử dụng Junit:DBConnectTest.java, ScheduleTest.java

* ** Nguyễn Đình Quang **
* 1.1: Thiết kế các test case(review cho Quyết, Saroeun, Senglao)
* 1.2: Lập trình các class testsuite sử dụng Junit:AdminLoginTest.java, CreateCoursesTest.java

5.Đánh giá phần trăm công việc

* Nguyễn Văn Quyết : 20%
* Yos Saroeun : 20%
* Kerlor Senglao : 20%
* Nguyễn Đình Quang: 40%


* ** File báo cáo: Trong thư mục Report PDF File và Report Ofice File  **
* ** Link Demo Project: https://www.youtube.com/watch?v=m3Y-4xaCV8Y&t=3s **

- - -
6.Danh sách thành viên trong nhóm TKXDPM.20181-17

 |       Họ tên     | MSSV     |
 |:-----------------|:--------:|
 |Nguyễn Đình Quang | 20146574 |
 |Nguyễn Văn Quyết  | 20153075 | 
 |Yos Saroeun       | 20154461 | 
 |Kerlor Senglao    | 20154474 | 
 
